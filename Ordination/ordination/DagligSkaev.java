package ordination;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;

public class DagligSkaev extends Ordination {
	private ArrayList<Dosis> doser = new ArrayList<>();

	public DagligSkaev(LocalDate startDen, LocalDate slutDen) {
		super(startDen, slutDen);
	}

	public ArrayList<Dosis> getDoser() {
		return doser;
	}

	public void opretDosis(double antal, LocalTime tid) {
		Dosis dosis = new Dosis(antal);
		dosis.setTid(tid);
		doser.add(dosis);
	}

	public void deleteDosis(Dosis dosis) {
		doser.remove(dosis);
	}

	@Override
	public double samletDosis() {
		double sum = 0;
		for (int j = 0; j < doser.size(); j++) {
			sum += doser.get(j).getAntal();
		}
		return sum * antalDage();
	}

	@Override
	public double doegnDosis() {
		double avgsum = 0;
		for (int i = 0; i < doser.size(); i++) {
			avgsum += doser.get(i).getAntal();
		}
		return avgsum;
	}

	@Override
	public String getType() {
		return "DagligSkaev";
	}

}
