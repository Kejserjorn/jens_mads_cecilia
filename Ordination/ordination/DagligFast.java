package ordination;

import java.time.LocalDate;

public class DagligFast extends Ordination
{
    private Dosis[] doser = new Dosis[4];

    public DagligFast(LocalDate startDen, LocalDate slutDen)
    {
	super(startDen, slutDen);
    }

    public Dosis[] getDoser()
    {
	return doser;
    }

    public Dosis createMorgenDosis(double antal)
    {
	Dosis dosis = new Dosis(antal);
	doser[0] = dosis;
	return dosis;
    }

    public Dosis createMiddagsDosis(double antal)
    {
	Dosis dosis = new Dosis(antal);
	doser[1] = dosis;
	return dosis;
    }

    public Dosis createAftenDosis(double antal)
    {
	Dosis dosis = new Dosis(antal);
	doser[2] = dosis;
	return dosis;
    }

    public Dosis createNatDosis(double antal)
    {
	Dosis dosis = new Dosis(antal);
	doser[3] = dosis;
	return dosis;
    }

    @Override
    public double samletDosis()
    {
	return doegnDosis() * antalDage();
    }

    @Override
    public double doegnDosis()
    {
	double avgsum = 0;
	for (int j = 0; j < doser.length; j++)
	{
	    avgsum += doser[j].getAntal();
	}

	return avgsum;
    }

    @Override
    public String getType()
    {
	return "DagligFast";
    }
}
