package ordination;

import java.time.LocalDate;
import java.util.ArrayList;

public class Patient
{
    private String cprnr;
    private String navn;
    private double vaegt;

    private ArrayList<Ordination> ordinationer = new ArrayList<>();

    public Patient(String cprnr, String navn, double vaegt)
    {
	this.cprnr = cprnr;
	this.navn = navn;
	this.vaegt = vaegt;
    }

    public String getCprnr()
    {
	return cprnr;
    }

    public String getNavn()
    {
	return navn;
    }

    public void setNavn(String navn)
    {
	this.navn = navn;
    }

    public double getVaegt()
    {
	return vaegt;
    }

    public void setVaegt(double vaegt)
    {
	this.vaegt = vaegt;
    }

    // TODO: Metoder (med specifikation) til at vedligeholde link til Ordination

    public ArrayList<Ordination> getOrdinationer()
    {
	return ordinationer;
    }

    public void addOrdination(Ordination ordination)
    {
	ordinationer.add(ordination);
    }

    public DagligFast createOrdinationDagligFast(LocalDate startDen,
	    LocalDate slutDen)
    {
	DagligFast o = new DagligFast(startDen, slutDen);
	ordinationer.add(o);
	return o;
    }

    public DagligSkaev createOrdinationDagligSkaev(LocalDate startDen,
	    LocalDate slutDen)
    {
	DagligSkaev o = new DagligSkaev(startDen, slutDen);
	ordinationer.add(o);
	return o;
    }

    public PN createOrdinationPN(LocalDate startDen, LocalDate slutDen,
	    double antalEnheder)
    {
	PN o = new PN(startDen, slutDen, antalEnheder);
	ordinationer.add(o);
	return o;
    }

    @Override
    public String toString()
    {
	return navn + "  " + cprnr;
    }

}
