package ordination;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.time.LocalDate;

import org.junit.Test;

public class PNTest {

	// Test til givDosis metode
	@Test
	public void pn_GivDosis_ReturnTrue() {
		LocalDate startDato = LocalDate.of(2016, 04, 01);
		LocalDate slutDato = LocalDate.of(2016, 05, 01);
		LocalDate givDen = LocalDate.of(2016, 04, 05);
		PN tester = new PN(startDato, slutDato, 5);
		boolean testResult = tester.givDosis(givDen);
		assertTrue(testResult);
	}

	@Test
	public void pn_GivDosis_ReturnFalse_FørStart() {
		LocalDate startDato = LocalDate.of(2016, 04, 01);
		LocalDate slutDato = LocalDate.of(2016, 05, 01);
		LocalDate givDen = LocalDate.of(2016, 03, 30);
		PN tester = new PN(startDato, slutDato, 5);
		boolean testResult = tester.givDosis(givDen);
		assertFalse(testResult);
	}

	@Test
	public void pn_GivDosis_ReturnFalse_EfterSlutDato() {
		LocalDate startDato = LocalDate.of(2016, 04, 01);
		LocalDate slutDato = LocalDate.of(2016, 05, 02);
		LocalDate givDen = LocalDate.of(2016, 05, 06);
		PN tester = new PN(startDato, slutDato, 5);
		boolean testResult = tester.givDosis(givDen);
		assertFalse(testResult);
	}

	// Test til doegnDosis metode
	@Test
	public void pn_doegnDosis_SammeDag_Return1komma667() {
		LocalDate startDato = LocalDate.of(2016, 04, 01);
		LocalDate slutDato = LocalDate.of(2016, 05, 01);
		LocalDate givDen1 = LocalDate.of(2016, 04, 10);
		LocalDate givDen2 = LocalDate.of(2016, 04, 10);
		PN tester = new PN(startDato, slutDato, 25);
		tester.givDosis(givDen1);
		tester.givDosis(givDen2);
		double testResult = tester.doegnDosis();
		assertEquals(1.6666666666666667, testResult, 0.0001);
	}

	@Test
	public void pn_doegnDosis_ForskelligeDage_Return1komma667() {
		LocalDate startDato = LocalDate.of(2016, 04, 01);
		LocalDate slutDato = LocalDate.of(2016, 05, 01);
		LocalDate givDen1 = LocalDate.of(2016, 04, 10);
		LocalDate givDen2 = LocalDate.of(2016, 04, 12);
		PN tester = new PN(startDato, slutDato, 25);
		tester.givDosis(givDen1);
		tester.givDosis(givDen2);
		double testResult = tester.doegnDosis();
		assertEquals(1.6666666666666667, testResult, 0.0001);
	}

	@Test
	public void pn_doegnDosis_dosisFørStart_Return0() {
		LocalDate startDato = LocalDate.of(2016, 04, 01);
		LocalDate slutDato = LocalDate.of(2016, 05, 01);
		LocalDate givDen1 = LocalDate.of(2016, 03, 10);
		PN tester = new PN(startDato, slutDato, 25);
		tester.givDosis(givDen1);
		double testResult = tester.doegnDosis();
		assertEquals(0.0, testResult, 0.0001);
	}

	@Test
	public void pn_doegnDosis_dosisEfterSlut_Return0() {
		LocalDate startDato = LocalDate.of(2016, 04, 01);
		LocalDate slutDato = LocalDate.of(2016, 05, 01);
		LocalDate givDen1 = LocalDate.of(2016, 05, 02);
		PN tester = new PN(startDato, slutDato, 25);
		tester.givDosis(givDen1);
		double testResult = tester.doegnDosis();
		assertEquals(0.0, testResult, 0.00001);
	}

	// Test til samletDosis metode
	@Test
	public void pn_samletDosis_TreEnsDatoer_Return369() {
		LocalDate startDato = LocalDate.of(2016, 04, 01);
		LocalDate slutDato = LocalDate.of(2016, 05, 01);
		LocalDate givDen1 = LocalDate.of(2016, 04, 10);
		LocalDate givDen2 = LocalDate.of(2016, 04, 10);
		LocalDate givDen3 = LocalDate.of(2016, 04, 10);
		PN tester = new PN(startDato, slutDato, 123);
		tester.givDosis(givDen1);
		tester.givDosis(givDen2);
		tester.givDosis(givDen3);
		double testResult = tester.samletDosis();
		assertEquals(369.0, testResult, 0.0001);
	}

	@Test
	public void pn_samletDosis_TreForskelligeDatoer_Return369() {
		LocalDate startDato = LocalDate.of(2016, 04, 01);
		LocalDate slutDato = LocalDate.of(2016, 05, 01);
		LocalDate givDen1 = LocalDate.of(2016, 04, 10);
		LocalDate givDen2 = LocalDate.of(2016, 04, 11);
		LocalDate givDen3 = LocalDate.of(2016, 04, 12);
		PN tester = new PN(startDato, slutDato, 123);
		tester.givDosis(givDen1);
		tester.givDosis(givDen2);
		tester.givDosis(givDen3);
		double testResult = tester.samletDosis();
		assertEquals(369.0, testResult, 0.0001);
	}

	// Irrelevante tests
	@Test
	public void pn_getAntalGangeGivet_Return2() {
		LocalDate startDato = LocalDate.of(2016, 04, 01);
		LocalDate slutDato = LocalDate.of(2016, 05, 01);
		LocalDate givDen1 = LocalDate.of(2016, 04, 10);
		LocalDate givDen2 = LocalDate.of(2016, 04, 12);
		PN tester = new PN(startDato, slutDato, 25);
		tester.givDosis(givDen1);
		tester.givDosis(givDen2);
		int testResult = tester.getAntalGangeGivet();
		assertEquals(2, testResult, 0.001);
	}

}