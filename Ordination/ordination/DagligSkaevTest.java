package ordination;

import static org.junit.Assert.assertEquals;

import java.time.LocalDate;
import java.time.LocalTime;

import org.junit.Test;

public class DagligSkaevTest {
	DagligSkaev ds1 = new DagligSkaev(LocalDate.of(2016, 6, 15), LocalDate.of(2016, 6, 20));

	@Test
	public void testOpretDosis() {
		ds1.opretDosis(5, LocalTime.of(12, 30));
		assertEquals(5, ds1.getDoser().get(0).getAntal(), 0.0001);
	}

	@Test
	public void testSamletDosis() {
		ds1.opretDosis(5, LocalTime.of(12, 50));
		assertEquals(5 * ds1.antalDage(), ds1.samletDosis(), 0.0001);
	}

	@Test
	public void testDøgnDosis() {
		ds1.opretDosis(5, LocalTime.of(12, 50));
		assertEquals(5, ds1.doegnDosis(), 0.0001);
	}

	@Test
	public void testDøgnDosis2() {
		ds1.opretDosis(5, LocalTime.of(12, 50));
		ds1.opretDosis(5, LocalTime.of(14, 50));
		assertEquals(10, ds1.doegnDosis(), 0.0001);
	}

}
