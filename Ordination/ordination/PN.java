package ordination;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;

public class PN extends Ordination
{
    private double antalEnheder;
    private int antalGangeGivet;
    private ArrayList<LocalDate> doser = new ArrayList<>();

    public PN(LocalDate startDen, LocalDate slutDen, double antalEnheder)
    {
	super(startDen, slutDen);
	this.antalEnheder = antalEnheder;
    }

    public double getAntalEnheder()
    {
	return antalEnheder;
    }

    public void setAntalEnheder(double antalEnheder)
    {
	if (antalEnheder <= 0)
	{
	    throw new IndexOutOfBoundsException();

	}
	else
	{
	    this.antalEnheder = antalEnheder;
	}
    }

    /**
     * Registrerer at der er givet en dosis paa dagen givesDen Returnerer true
     * hvis givesDen er inden for ordinationens gyldighedsperiode og datoen
     * huskes Retrurner false ellers og datoen givesDen ignoreres
     *
     * @param givesDen
     * @return
     */

    public boolean givDosis(LocalDate givesDen)
    {
	if (givesDen.isAfter(getStartDen()) && givesDen.isBefore(getSlutDen()))
	{
	    doser.add(givesDen);
	    antalGangeGivet++;
	    return true;
	}
	else
	{
	    return false;
	}
    }

    @Override
    public double doegnDosis()
    {
	// Denne metode er problemet!
	double doegn = (antalGangeGivet * getAntalEnheder())
		/ ChronoUnit.DAYS.between(getStartDen(), getSlutDen());
	return doegn;
    }

    @Override
    public double samletDosis()
    {
	double samlet = antalGangeGivet * getAntalEnheder();
	return samlet;
    }

    /**
     * Returnerer antal gange ordinationen er anvendt
     *
     * @return
     */
    public int getAntalGangeGivet()
    {
	return antalGangeGivet;
    }

    @Override
    public String getType()
    {
	return "PN";
    }

}
