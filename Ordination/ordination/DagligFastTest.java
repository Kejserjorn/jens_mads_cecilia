package ordination;

import static org.junit.Assert.assertEquals;

import java.time.LocalDate;

import org.junit.Test;

public class DagligFastTest {
	DagligFast df1 = new DagligFast(LocalDate.of(2016, 6, 15), LocalDate.of(2016, 6, 20));

	@Test
	public void testDoegnDosis() {
		df1.createMorgenDosis(5);
		df1.createMiddagsDosis(2);
		df1.createAftenDosis(3);
		df1.createNatDosis(2);
		assertEquals(12, df1.doegnDosis(), 0.0001);
	}

	@Test
	public void testSamletDosis() {
		df1.createMorgenDosis(5);
		df1.createMiddagsDosis(2);
		df1.createAftenDosis(3);
		df1.createNatDosis(2);
		assertEquals(12 * df1.antalDage(), df1.samletDosis(), 0.0001);
	}

}
