package service;

import java.time.LocalDate;
import java.time.LocalTime;

import org.junit.Test;

import ordination.Laegemiddel;
import ordination.Patient;

public class ServiceTest
{
    private LocalDate first = LocalDate.of(2016, 9, 18);
    private LocalDate second = LocalDate.of(2016, 9, 19);
    private Patient hans = new Patient("12345678", "Hans", 100);
    private Laegemiddel lm = new Laegemiddel("Fucidin", 0.025, 0.025, 0.025,
	    "Styk");
    private Service s = Service.getTestService();

    /**
     * Test method for
     * {@link service.Service#opretPNOrdination(java.time.LocalDate, java.time.LocalDate, ordination.Patient, ordination.Laegemiddel, double)}
     * .
     */
    @Test(expected = IllegalArgumentException.class)
    public void testOpretPNOrdinationNegativeDays()
    {
	s.opretPNOrdination(second, first, hans, lm, 5);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testOpretPNOrdinationNegativeNumber()
    {
	s.opretPNOrdination(first, second, hans, lm, -5);
    }

    @Test
    public void testOpretPNOrdination()
    {
	s.opretPNOrdination(first, second, hans, lm, 5);
    }

    /**
     * Test method for
     * {@link service.Service#opretDagligFastOrdination(java.time.LocalDate, java.time.LocalDate, ordination.Patient, ordination.Laegemiddel, double, double, double, double)}
     * .
     */
    @Test(expected = IllegalArgumentException.class)
    public void testOpretDagligFastOrdinationNegativeDays()
    {
	s.opretDagligFastOrdination(second, first, hans, lm, 5, 5, 5, 2);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testOpretDagligFastOrdinationNegativeNumber()
    {
	s.opretDagligFastOrdination(first, second, hans, lm, 5, -5, 5, 2);
    }

    @Test
    public void testOpretDagligFastOrdination()
    {
	s.opretDagligFastOrdination(first, second, hans, lm, 5, 5, 5, 2);
    }

    /**
     * Test method for
     * {@link service.Service#opretDagligSkaevOrdination(java.time.LocalDate, java.time.LocalDate, ordination.Patient, ordination.Laegemiddel, java.time.LocalTime[], double[])}
     * .
     */

    @Test(expected = IllegalArgumentException.class)
    public void testOpretDagligSkaevOrdinationNegativeDays()
    {
	LocalTime[] klok =
	{ LocalTime.of(12, 00), LocalTime.of(15, 30) };
	double[] antal =
	{ 5, 2, 5, 5 };
	s.opretDagligSkaevOrdination(second, first, hans, lm, klok, antal);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testOpretDagligSkaevOrdinationNegativeNumber()
    {
	LocalTime[] klok =
	{ LocalTime.of(12, 00), LocalTime.of(15, 30) };
	double[] antal =
	{ 5, -2, 5, 5 };
	s.opretDagligSkaevOrdination(first, second, hans, lm, klok, antal);
    }

    @Test
    public void testOpretDagligSkaevOrdination()
    {
	LocalTime[] klok =
	{ LocalTime.of(12, 00), LocalTime.of(15, 30) };
	double[] antal =
	{ 5, 2, 5, 5 };
	s.opretDagligSkaevOrdination(first, second, hans, lm, klok, antal);
    }
}
